import socket
import re

refer_strs = ["refer", "ReferralServer"]

quirks_pre = {"whois.arin.net": b"a "}
quirks_post = {"whois.ripe.net": b" -B", "whois.afrinic.net": b" -B"}
quirks_noas = ["whois.arin.net"]

email_fields = ["OrgAbuseEmail", "OrgTechEmail", "RTechEmail", "OrgNOCEmail", "e-mail"]
ignore = ["helpdesk@apnic.net"]

abuse_re = re.compile(
    r"^% Abuse contact for 'AS[0-9]{1,6}' is '([^']+)'$", re.MULTILINE
)


def getwhoisfield(data: str, field: str, toignore: list = []) -> list:
    response = []
    if field in data:
        for line in data.split("\n"):
            if not line.startswith(field):
                continue
            fieldvalue = line.split(field + ": ")[-1].strip()

            if fieldvalue in ignore:
                continue

            response.append(line.split(field + ": ")[-1].strip())
    return response


def whois(query: str, server: str = "whois.iana.org", port: int = 43):
    quirk_query = quirks_pre.get(server, b"") + query.encode() + quirks_post.get(server, b"")
    # Thanks arin <3
    if server in quirks_noas:
        quirk_query = quirk_query.replace(b"AS", b"")

    with socket.create_connection((server, port)) as s:
        s.sendall(quirk_query + b"\r\n")
        data = b""
        while True:
            newdata = s.recv(1024)
            data += newdata
            if not newdata:
                break

    data = data.decode("latin_1")

    # HELL
    for refer_str in refer_strs:
        referred_servers = getwhoisfield(data, refer_str)
        if not referred_servers:
            continue
        # I do not want rwhois.
        elif referred_servers[0].startswith("rwhois://"):
            continue

        referred_server = referred_servers[0].replace("whois://", "")
        print(f"Forwarded to {referred_server}")
        if ":" in referred_server:
            new_server, new_port = referred_server.split(":")
            return whois(query, server=new_server, port=new_port)
        else:
            return whois(query, server=referred_server)

    return data


def getwhoisemails(data: str):
    emails = []
    for email_field in email_fields:
        emails += getwhoisfield(data, email_field)

    emails += abuse_re.findall(data)

    # Remove dupes
    emails = list(set(emails))

    return emails


# whoisresult = whois(sys.argv[1])
# whoisemails = getwhoisemails(whoisresult)
# print(whoisresult)
# print(whoisemails)

