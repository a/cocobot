import discord
from discord.ext.commands import Cog
from discord.ext import commands
import re
import findasncontacts
import asyncio
import secrets
import aiohttp


class Verify(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.asn_regex = re.compile(r"^(AS|as)?(\d{1,6})$")

    def check_asn_validity(self, asn_in):
        asnlist = self.asn_regex.findall(asn_in)
        if not asnlist:
            return False, "No ASN found"
        asn = int(asnlist[0][1])
        # Block out bogons
        # from http://bgpfilterguide.nlnog.net/guides/bogon_asns/
        # TY for the great resource, NLNOG <3
        if (
            (asn in [0, 23456, 65535, 4294967295])
            or (asn >= 64496 and asn <= 64511)
            or (asn >= 64512 and asn <= 65534)
            or (asn >= 4200000000 and asn <= 4294967294)
            or (asn >= 65552 and asn <= 131071)
        ):
            return False, "ASN is a bogon"
        # I'm stripping the number and re-prepending AS
        # so that I can account for ASNs without AS before them
        return True, f"AS{asn}"

    def add_asn_to_nick(self, name, nick, asn):
        # Drop ASNs from names (to avoid impersonation)
        if not nick:
            nick = name.split("-")[0].strip()

        # Decode the existing ASNs in a VERY cursed way
        if " - " in nick:
            asnpart = (
                nick.split(" - ")[1]
                .strip()
                .upper()
                .replace(", ", ",")
                .replace(" ", ",")
                .replace("/", ",")
            )
            asns = asnpart.split(",")
            nick = nick.split(" - ")[0]
        else:
            asns = []

        if asn in asns:
            return False, "You are already verified for this ASN"

        asns.append(asn)

        asnstr = " - " + ", ".join(asns)

        # Allow of for av... - AS123456 etc at most
        if len(asnstr) > 27:
            return False, "Can't verify for this ASN, would truncate username too much."

        # If the nick + asn string combo is too long, truncate nicknaem
        if len(nick + asnstr) > 32:
            nick = nick[: -((len(nick + asnstr) - 32) + 3)]
            return "truncated", (nick + "..." + asnstr)

        return True, (nick + asnstr)

    @commands.command(aliases=["verify", "verifyasn"])
    @commands.cooldown(1, 60, type=commands.BucketType.user)
    @commands.guild_only()
    async def asnverify(self, ctx, asn: str):
        """Verifies your ASN to give you the role."""
        # Do basic ASN verification
        asn_valid, validity_result = self.check_asn_validity(asn)
        if not asn_valid:
            return await ctx.send(f":no_entry: {validity_result}.")
        asn = validity_result

        nick_ok, nick_result = self.add_asn_to_nick(
            ctx.author.name, ctx.author.nick, asn
        )
        if not nick_ok:
            return await ctx.send(f":no_entry: {nick_result}.")

        # ctx.send("Heyo, please reply with one of the following verification options:\n"
        #          "- \"peeringdb\": You'll be given a URL to do OAUTH with peeringdb, which will give us a list of your organization affiliations (see them here: https://www.peeringdb.com/profile)\n"
        #          "- \"email\": An email with a verification code will be sent to an email address from the ASN's whois (you'll be able to pick one in the next step)\n"
        #          "- \"manual\": (Recommended for large organizations) A manual verification will be performed by a BGPeople staff member to help you verify your ASN")

        # def check(m):
        #     return (
        #         m.content.strip() in ["peeringdb", "email", "manual"]
        #         and m.channel == ctx.channel
        #         and m.author == ctx.author
        #     )

        # msg = await self.bot.wait_for("message", timeout=60.0, check=check)

        whoisresult = findasncontacts.whois(asn)
        whoisemails = findasncontacts.getwhoisemails(whoisresult)

        # Check if ASN is a LIR
        whois_is_lir = findasncontacts.getwhoisfield(whoisresult, "org-type")
        is_lir = (whois_is_lir and whois_is_lir[0] == "LIR")

        if not whoisemails:
            return await ctx.send(
                f"{ctx.author.mention}: No emails found in whois. Either add an email address to this ASN's whois, or do a manual verification by contacting a mod."
            )

        emailstr = "\n- " + "\n- ".join(whoisemails)
        # Clean emails
        emailstr = await commands.clean_content(escape_markdown=True).convert(
            ctx, emailstr
        )

        nick_notice = (
            f"\n\nIf you complete this verificatiton, your nickname will be truncated to: {nick_result}"
            if nick_ok == "truncated"
            else ""
        )

        # Clean nickname truncation
        nick_notice = await commands.clean_content(escape_markdown=True).convert(
            ctx, nick_notice
        )

        await ctx.send(
            f"{ctx.author.mention}: I found the following email addresses from whois entries for that ASN, please reply in this channel with one of them in the next 60 seconds: {emailstr}\n"
            'Send "cancel" if you want to cancel this verification attempt.\n\n'
            f"If you don't have access to any of these email addresses but are still affiliated with this ASN, please contact a mod to do a manual verification.{nick_notice}"
        )

        def email_check(m):
            return (
                m.content.strip() in (whoisemails + ["cancel"])
                and m.channel == ctx.channel
                and m.author == ctx.author
            )

        try:
            msg = await self.bot.wait_for("message", timeout=60.0, check=email_check)
        except asyncio.TimeoutError:
            return await ctx.send(
                f"{ctx.author.mention}: You didn't reply in time. Please run this command again if you want to re-attempt verification."
            )

        email = msg.content.strip()

        if email == "cancel":
            return await ctx.send(f"{ctx.author.mention}: Cancelled verification.")

        verif_code = f"verifyme-{asn}-{secrets.token_hex(16)}"

        resp = await self.bot.aiosession.post(
            f"https://api.eu.mailgun.net/v3/{self.bot.config['mailgun']['domain']}/messages",
            auth=aiohttp.BasicAuth(
                login="api", password=self.bot.config["mailgun"]["apikey"],
            ),
            data={
                "from": self.bot.config["mailgun"]["from"],
                "to": [email],
                "subject": f"{self.bot.config['mailgun']['guildname']} verification code for {asn}",
                "text": (
                    "Hello!\n\n"
                    f'You\'re receiving this email because someone with the Discord username "{str(ctx.author)}" is trying to verify themselves as being affiliated with {asn}.\n\n'
                    f"If you requested this, please go back to the channel where you're trying to do the verification and send this message: {verif_code}\n\n"
                    "If you didn't request this, simply ignore this email as the code will be invalidated in 60 minutes.\n\n"
                    "Have a nice day,\n"
                    f"{self.bot.config['mailgun']['botname']}"
                ),
            },
        )

        if resp.status != 200:
            response = await resp.text()
            self.bot.log.error(f"{resp.status} - {response}")
            return await ctx.send(
                f"{ctx.author.mention}: Uh oh, the bot had an error with the email submission. This is on bot's end, not yours. Please inform a staff member to check the logs."
            )

        await ctx.send(
            f"{ctx.author.mention}: I've emailed you at {email} about verifying {asn}. Please post the code you got here in the next 60 minutes to verify your affiliation with this ASN.\n"
            'Send "cancel" if you want to cancel this verification attempt.'
        )

        def code_check(m):
            return (
                (
                    verif_code.lower() in m.content.lower()
                    or m.content.strip() == "cancel"
                )
                and m.channel == ctx.channel
                and m.author == ctx.author
            )

        # TODO: resend option maybe?

        try:
            msg = await self.bot.wait_for("message", timeout=3600.0, check=code_check)
        except asyncio.TimeoutError:
            return await ctx.send(
                f"{ctx.author.mention}: No valid reply received in time. Please run this command again if you want to re-attempt verification."
            )

        if msg.content.strip() == "cancel":
            return await ctx.send(f"{ctx.author.mention}: Cancelled verification.")

        # Give user the "Verified ASN" role if they don't have it already
        role = discord.utils.get(
            ctx.guild.roles, id=self.bot.config["cocobot"]["verifiedasnrole"]
        )
        if role not in ctx.author.roles:
            await ctx.author.add_roles(role)

        # Give user the "RIPE LIR" role if they're a LIR
        if is_lir:
            role = discord.utils.get(
                ctx.guild.roles, id=self.bot.config["cocobot"]["ripelirrole"]
            )
            if role not in ctx.author.roles:
                await ctx.author.add_roles(role)

        await ctx.author.edit(nick=nick_result)

        return await ctx.send(f"{ctx.author.mention}: Verification complete.")


def setup(bot):
    bot.add_cog(Verify(bot))
